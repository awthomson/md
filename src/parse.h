int parse(char *fname);
void format_line(char *text, char *output);
int get_word(char *text, char *word, char *prefix, char *suffix, char *whitespace);
int starts_with(char *text, const char* sub);