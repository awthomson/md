Line 1 - This is a *single* astrix
Line 1.1 - This is a **double** astrix
Line 2 - This is a _single_ underscore
LIne 2.1 - how about a __double__ underscore?
Line 3- this is a `code` test
Line 4 - ...and this is a ~~strikethrough~~ test

This is a _multi-
-line italic_ example

How about ``some formatting inside a **code** statement`` ?

# Header down at the bottom

Some normal text here

## Try a sub-header

This is more more normal text
