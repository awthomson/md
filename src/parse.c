#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include "parse.h"

int emA, emB, strongA, strongB, strike, code;


// ===- Per word -=====================================
// _ or *			emphasis
// __ or **			strong
// ~~				strikethrough
// ` or ``			inline code

// ===- Per line -=====================================
// --... or ==...	Underlined title
// #...				Heading
// ```...			Code block



int parse(char *fname) {
	
	emA = 0;
	emB = 0;
	strongA = 0;
	strongB = 0;
	strike = 0;
	
	struct winsize max;
    ioctl(0, TIOCGWINSZ , &max);
    printf ("lines %d\n", max.ws_row);
    printf ("columns %d\n", max.ws_col);
	
	char txt[1000];
	char tmp_str[1000];
	// char *tmp_str = (char*)malloc(1000*sizeof(char));
	FILE *fptr;
	fptr = fopen(fname,"r");
	if (fptr == NULL) {
		printf("Error! opening file");
		return 1;
	}
	
	// Read in a whole line
	while( fgets(txt, 1000, fptr) != NULL) {
		format_line(txt, tmp_str);
		printf("%s", tmp_str);
	}
	
   fclose(fptr);   
   return 0;
}

int starts_with(char *text, const char* sub) {
	if (strncmp(text, sub, strlen(sub))==0)
		return 1;
	else
		return 0;
}

void format_line(char *text, char *output) {
	int out_line_pos = 0;
	int ptr = 0;
	
	// TODO: Handle full-line scarios here (code blocks, headings, underlines, etc.)
	if (starts_with(text, "#")) {
		sprintf(output, "\e[1m\e[3m\e[1;33m%s\e[0m", text);
		return;
	} else if (starts_with(text, "#")) {
		sprintf(output, "\e[1m\e[1;33m%s\e[0m", text);
		return;
	}
	
	// Stream the line and parse it word at a time
	char *word; word = (char*)malloc(1000*sizeof(char));
	char *whitespace; whitespace = (char*)malloc(1000*sizeof(char));
	char *suffix; suffix = (char*)malloc(1000*sizeof(char));
	char *prefix; prefix = (char*)malloc(1000*sizeof(char));
	while (ptr < strlen(text)) {
		// TODO: No formatting inside code statement/block
		int span = get_word(&text[ptr], word, prefix, suffix, whitespace);

		int n=0;
		if (strcmp(prefix, "_")==0) {
			emA++;
		} else if (strcmp(prefix, "__")==0) {
			strongA++;
		} else if (strcmp(prefix, "*")==0) {
			emB++;
		} else if (strcmp(prefix, "**")==0) {
			strongB++;
		} else if (strcmp(prefix, "~~")==0) {
			strike++;
		} else if ((strcmp(prefix, "`")==0) || (strcmp(prefix, "``")==0)) {
			code++;
		}
		out_line_pos+=n;
		
		/*
		n = sprintf(&output[out_line_pos], "\e[0m");
		out_line_pos += n;
		*/
			
		// Add any text control characters
		if (emA || emB) {
			n = sprintf(&output[out_line_pos], "\e[3m");
			out_line_pos += n;
		}
		if (strongA || strongB) {
			n = sprintf(&output[out_line_pos], "\e[1m");
			out_line_pos += n;
		}
		if (code) {
			n = sprintf(&output[out_line_pos], "\033[0;32m");
			out_line_pos += n;
		}
		if (strike) {
			n = sprintf(&output[out_line_pos], "\e[9m");
			out_line_pos += n;
		}

		n = 0;
		if (!code)
			n = sprintf(&output[out_line_pos], "%s", word);
		else
			n = sprintf(&output[out_line_pos], "%s%s%s",  prefix, word, suffix);
		out_line_pos+=n;

		n=0;
		if (strcmp(suffix, "_")==0) {
			emA--;
			if (emA == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		} else if (strcmp(suffix, "__")==0) {
			strongA--;
			if (strongA == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		} else if (strcmp(suffix, "*")==0) {
			emB--;
			if (emB == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		} else if (strcmp(suffix, "**")==0) {
			strongB--;
			if (strongB == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		} else if (strcmp(suffix, "~~")==0) {
			strike--;
			if (strike == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		} else if ((strcmp(suffix, "`")==0) || (strcmp(suffix, "``")==0)) {
			code--;
			if (strike == 0) {
				n = sprintf(&output[out_line_pos], "\e[0m");
			}
		}
		out_line_pos+=n;

		n = sprintf(&output[out_line_pos], "%s", whitespace);
		out_line_pos+=n;
		ptr+=span;
	}
	free(suffix);
	free(prefix);
	free(word);
	free(whitespace);
	
}

int get_word(char *text, char *word, char *prefix, char *suffix, char *whitespace) {
	int pos = 0;
	int word_pos = 0;
	int prefix_pos = 0;
	int suffix_pos = 0;
	int ws_pos = 0;
	
	for (int i=0; i<1000; i++) {
		prefix[i]=0;
		suffix[i]=0;
		word[i]=0;
		whitespace[i]=0;
	}
	
	// Ingest prefix
	while ((text[pos]=='_') || (text[pos]=='*') || (text[pos]=='~') || (text[pos]=='`'))
		prefix[prefix_pos++]=text[pos++];

	int t_pos = pos;
	
	// Ingest trailing whitespace
	// Find end of current word
	while((text[t_pos]!=' ') && (text[t_pos]!='\n') && (text[t_pos]!='\t'))
		t_pos++;
	int t_pos_2 = t_pos-1;
	// Track backwards gobbling up special chars
	while ((text[t_pos_2]=='_') || (text[t_pos_2]=='*') || (text[t_pos_2]=='~') || (text[t_pos_2]=='`'))
		suffix[suffix_pos++]=text[t_pos_2--];
	
	// Ingest following whitespace (t_pos will be the start of the next word at the end)
	while((text[t_pos]==' ') || (text[t_pos]=='\n') || (text[t_pos]=='\t'))
		whitespace[ws_pos++] = text[t_pos++];
	
	// Ingest word
	while (pos<=t_pos_2) {
		word[word_pos++]=text[pos++];
	}
	
	return t_pos;
}
